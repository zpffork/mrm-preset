# @zpf518518/mrm-preset

...

## Changelog

The changelog can be found on the [Releases page](ssh://git@gitlab.com/zpffork/mrm-preset/releases).

## Contributing

Everyone is welcome to contribute. Please take a moment to review the [contributing guidelines](Contributing.md).

## Authors and license

[zpf518518](https://www.npmjs.com/package/@zpf518518/mrm-preset) and [contributors](ssh://git@gitlab.com/zpffork/mrm-preset/graphs/contributors).

MIT License, see the included [License.md](License.md) file.
